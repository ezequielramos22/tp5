unit uAproximadorF; //Version 20:45hs.

{$mode objfpc}{$H+} {$M+} //directivas para trabajar con objet
interface

type

  tVector = array of double;
  tMatriz = array of tVector;

TAproximadorF=class
      private          //por ejemplo si la funcion es cos(x) entonces: 
          x: tVector;  // x=0 | x=pi/2  | x=pi
          f: tVector;  // f=1;| f=0     | f=-1  
          cantF: integer;   // Atributos opcionales
          cantC: integer;
          matriz :tMatriz; //este atributo para la parte de osculacion

          function prodMatricial(A:tMatriz; V:tVector):tVector;
      public
          constructor crear(x0,f0:tVector; mtz:tMatriz);
          Destructor destruir();
          function newton(): tVector; 
          procedure lagrange(var mtz: tMatriz; var polinomio: tVector); 
          
      end;

      implementation 
         constructor TAproximadorF.crear(x0,f0:tVector; mtz:tMatriz); //todo ok
         begin
           
         end;

         Destructor TAproximadorF.destruir();
          begin
               inherited Destroy;
          end;

         function TAproximadorF.newton(): tVector;
           // es el newton para colocacion. utiliza los atributos x,f y devuelve 
           // el polinomio en un tipo de dato tVector.
          var 
            v:tVector;
          begin
              setlength(v,1);
              result := v;
          end;
          
           //le mandas este vector por ej V={1,-2} retorna R={-2,1,1} osea te multiplica todos los elementos q le mandes como polinomios
		  function multiplicador(V:tVector):tVector;
		  var
			R:tVector; D:double; i,j:byte;
		  begin
			setlength(R,length(V)+1);
			R[0]:=V[0]*(-1);
			R[1]:=1;
			for i:=1 to length(V)-1 do
			  begin
				for j:=i downto 0 do
				  R[j+1]:=R[j];
				R[0]:=0;
				D:=V[i]*(-1);
				for j:=0 to i do
				  R[j]:=(D*R[j+1])+R[j];
			  end;
			result:=R;
		  end; 
          
		  //producto de matriz nxn * vetor de n filas
          function TAproximadorF.prodMatricial(A:tMatriz; V:tVector):tVector;
          var
            AV:tVector;
            sum:double;
            fila,j:byte;
          begin
              setlength(AV,Length(V));
              for fila:=0 to high(A) do
              begin
                sum:=0;
                for j:=0 to high(A) do
                  begin
                       sum:=sum+A[fila][j]*V[j];
                  end;
                AV[fila]:=sum;
              end;
              result := AV;
          end;
          //Probando GitHub
          procedure TAproximadorF.lagrange(var mtz: tMatriz; var polinomio: tVector);  
            //se da por hecho que los x e y ya estan cargados en los atributos
            // x y f de este objeto. este metodo tiene que devolver los parametros
            // mtz y polinomio cargados es decir, con la solucion y la matriz lagrangeana. 
          begin
            
          end;
		
		procedure Lagrange(var vecx,vecy,pol:tvec; matriz:tMatriz);
		var n,i,j,c:byte; d:double; aux,temp:tVector;
		begin
			d:=1; n := high(vecx); SetLength(aux,n); SetLength(temp,n+1);
			for i := 0 to n do //cargo la matriz por columna.
			 begin
				c:=0;	
				for j := 0 to n do  //calculo cada li(x).
				begin
					if (i<>j) then
					begin
						d:=d*(vecx[i]-vecx[j]);
						inc(c);
						aux[c]:=vecx[j];
					end;
					//temp:=multicador(aux);
				end;
				for j := 0 to n do
					matriz[j,i]:=temp[j]/d;
			 end;
			 pol:=multiplicaMAtPorVec(matriz,vecy); 
		end;
      //producto de matriz nxn * vetor de n filas
      function prodMatricial(A:tMatriz; V:tVector):tVector;
      var
        AV:tVector;
        sum:double;
        f,j:byte;
      begin
          SetLength(AV,Length(V));
          for f:=0 to Length(A) do
          begin
            sum:=0;
            for j:=0 to Length(A) do
              begin
                   sum:=sum+A[f][j]*V[j];
              end;
            AV[f]:=sum;
          end;
          prodMatricial:=AV;
      end;

begin   
end. 